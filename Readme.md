Currency Pair Table
===============================

Development challenge for creating a sorted currency pair table.

Run

```
npm install
npm start
```

Once you've started the development server, navigate to http://localhost:8011
to read the task description and get started.
