/**
- * This javascript file contains the data manipulation functions.
+ * data.js
+ *
+ * @description
+ * Currency Pair Table data functions
+ * @author  Arda Durak arda.durak@gmail.com
+ * @updated 2017-02-12
 *
 */


/*
 * Function to be called for adding the new currency pair
 */
function addCurrency(currencyData, currencyArray) {
 // search if the currency pair already exists
  var currentItem = currencyArray.find(function (item) {
    return item.name === currencyData.name;
  });

  var newArray = [];

  if (currentItem) {
  // if the item exists already, swap it with the new object
    var index = currencyArray.indexOf(currentItem);
    newArray = newArray.concat(currencyArray.slice(0, index));
    newArray.push(createCurrencyObject(currencyData, currentItem));
    newArray = newArray.concat(currencyArray.slice(index + 1));
  } else {
  // if the item doesn't exists, simply add it to the array
    newArray = currencyArray;
    newArray.push(createCurrencyObject(currencyData));
  }

  return sortArray(newArray);
}

/*
 *  Function for creating the new currency pair object
 */

function createCurrencyObject(currencyData, currentItem) {
 // Upddate the midprice array
  var midpriceArray = currentItem ? filterPrices(currentItem.midpriceArray) : [];

  var newPrice = {
    time: Date.now(),
    value: calculateMidprice(currencyData)
  };
  midpriceArray.push(newPrice);

 // Create the new object
  var newCurrency = Object.assign({},
  currentItem || {},
  currencyData, {
    midpriceArray: midpriceArray
  }
 );

  return newCurrency;
}

/*
 *  Array and Price Functions
 */

function sortFunction(a, b) {
  return (a.lastChangeBid > b.lastChangeBid);
}

function sortArray(array) {
  return array.sort(sortFunction);
}

function filterFunction(a) {
  return (Date.now() - a.time) <= 30000;
}

function filterPrices(array) {
  return array.filter(filterFunction);
}

function calculateMidprice(data) {
  if (data && data.bestBid && data.bestAsk && !isNaN(data.bestBid) && !isNaN(data.bestAsk)) {
    return (data.bestBid + data.bestAsk) / 2;
  }
  return 0;
}

/*
 *  Other Functions
 */

function getTitles() {
  var titles = {
    name: 'Name',
    bestBid: 'Best Bid',
    bestAsk: 'Best Ask',
    openBid: 'Open Bid',
    openAsk: 'Open Ask',
    lastChangeAsk: 'Last Change Ask',
    lastChangeBid: 'Last Change Bid',
    sparkline: 'Graph'
  };

  return titles;
}

export default function () {
  var currencyArray = [];

  return {
    processData: function (currencyData) {
      currencyArray = addCurrency(currencyData, currencyArray);
    },
    getData: function () {
      return currencyArray;
    },
    getTitles: function () {
      return getTitles();
    }
  };
}
