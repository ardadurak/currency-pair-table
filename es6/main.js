/**
- * This javascript file contains the connection functions
+ * main.js
+ *
+ * @description
+ * Currency Pair Table connection functions
+ * @author  Arda Durak arda.durak@gmail.com
+ * @updated 2017-02-12
 *
 */

import render from './render';
import data from './data';

global.DEBUG = false;
const url = 'ws://localhost:8011/stomp';
const endpoint = '/fx/prices';
const targetId = 'currency-table';
const client = window.Stomp.client(url);
const dataFnc = data();
const renderFnc = render(targetId, dataFnc.getTitles());

function connect() {
  client.debug = function (msg) {
    if (global.DEBUG) {
      console.info(msg);
    }
  };

  client.connect({}, subscribe, function (error) {
    alert(error.headers.message);
  });
}

function subscribe() {
  client.subscribe(endpoint, processMessage);
}

function processMessage(message) {
  dataFnc.processData(JSON.parse(message.body));
  renderFnc.render(dataFnc.getData());
}

connect();
