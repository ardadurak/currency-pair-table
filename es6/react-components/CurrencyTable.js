import React from 'react';
import CurrencyTableHeader from './CurrencyTableHeader';
import CurrencyTableBody from './CurrencyTableBody';

// this is for rendering the whole table
class CurrencyTable extends React.Component {
  render() {
    return (
      <table className="table table-bordered table-striped table-hover table-responsive">
        <CurrencyTableHeader titles={this.props.titles} />
        <CurrencyTableBody data={this.props.data} />
      </table>);
  }
}

export default CurrencyTable;
