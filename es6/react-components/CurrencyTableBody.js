import React from 'react';
import CurrencyTableRow from './CurrencyTableRow';

// this is for rendering the body of the table
class CurrencyTableBody extends React.Component {
  render() {
    const data = this.props.data;
    const allRows = data.map(function (currency) {
      return <CurrencyTableRow key={currency.name} currency={currency} />;
    });
    return (
      <tbody>{allRows}</tbody>
    );
  }
}

export default CurrencyTableBody;
