import React from 'react';

// this is for rendering the header of the table
class CurrencyTableHeader extends React.Component {
  render() {
    return (<thead>
      <tr>
        <th>{this.props.titles.name}</th>
        <th>{this.props.titles.bestBid}</th>
        <th>{this.props.titles.bestAsk}</th>
        <th>{this.props.titles.openBid}</th>
        <th>{this.props.titles.openAsk}</th>
        <th>{this.props.titles.lastChangeAsk}</th>
        <th>{this.props.titles.lastChangeBid}</th>
        <th>{this.props.titles.sparkline}</th>
      </tr>
    </thead>);
  }
}

export default CurrencyTableHeader;
