import React from 'react';

// this is for rendering one single row of the table
class CurrencyTableRow extends React.Component {
  render() {
    return (
      <tr>
        <td>{this.props.currency.name}</td>
        <td>{this.props.currency.bestBid}</td>
        <td>{this.props.currency.bestAsk}</td>
        <td>{this.props.currency.openBid}</td>
        <td>{this.props.currency.openAsk}</td>
        <td>{this.props.currency.lastChangeAsk}</td>
        <td>{this.props.currency.lastChangeBid}</td>
        <td id={this.props.currency.name} />
      </tr>
    );
  }
}

export default CurrencyTableRow;
