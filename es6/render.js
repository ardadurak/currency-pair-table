/**
- * This javascript file contains the rendering functions (React).
+ * render.js
+ *
+ * @description
+ * Currency Pair Table rendering functions
+ * @author  Arda Durak arda.durak@gmail.com
+ * @updated 2017-02-12
 *
 */

import React from 'react';
import ReactDOM from 'react-dom';
import CurrencyTable from '../es6/react-components/CurrencyTable';

/*
 *  Sparkline functions
 */
function drawGraph(id, midpriceArray) {
 // this is for showing the midprice trend, green if the value is increasing, red otherwise
  var size = midpriceArray.length;
  var color = 'green';
  if (size > 1 && midpriceArray[size - 1] < midpriceArray[size - 2]) {
    color = 'red';
  }
  var options = {
    lineColor: color,
    endColor: 'black'
  };
  window.Sparkline.draw(document.getElementById(id), midpriceArray, options);
}

function drawPriceGraphs(currencyArray) {
  currencyArray.forEach(function (item) {
    var midpriceArray = item.midpriceArray.map(function (midPriceItem) {
      return midPriceItem.value;
    });
    drawGraph(item.name, midpriceArray);
  });
}

/*
 *  Rendering functions
 */
function render(el, data, titles) {
  ReactDOM.render(
    <CurrencyTable data={data} titles={titles} />,
  document.getElementById(el)
 );
  drawPriceGraphs(data);
}

export default function (containerId, titles) {
  return {
    render: function (data) {
      return render(containerId, data, titles);
    }
  };
}
