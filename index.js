/**
 * index.js
 *
 * @description
 * Currency Pair Table functions
 * @author  Arda Durak arda.durak@gmail.com
 * @updated 2017-02-12
 *
 */

 require('./site/index.html');
 require('./site/style.css');
 require('./es6/main');
